import React from 'react';
import ReactDOM from 'react-dom';
import ReactLoading from 'react-loading';
import { markdown } from 'markdown';
const fs = require("fs");

import styles from './styles.css';

import Joints from './joints';
import GraphicsEngine from './graphics';
import PoseNet from './posenet';
import {data_head} from './demo_util';


const descContent2 = fs.readFileSync("./head.json", "utf-8");

class App extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            webcam: true,
        }
    }


    async componentDidMount() {
        this.joints = new Joints();
        this.graphics_engine = new GraphicsEngine(this.refs.babylon, this.joints);
        this.posenet = new PoseNet(this.joints, this.graphics_engine, this.refs);
        const descContent = fs.readFileSync("./description.md", "utf-8");

        this.refs.description.innerHTML = markdown.toHTML(descContent);
        await this.posenet.loadNetwork();
        this.setState({loading: false});
        this.posenet.startPrediction().then((webcam) => {
            this.setState({ webcam });
        });
    }

    askWebCam(){
        this.posenet.startPrediction();
    }


    render() {
        return (
            <div id="container">
                <h2 className="text-center" id="h2">
                <button onClick={save}>Record</button>
                <button onClick={view}>View</button>
                </h2>
                <h5 id="h5">

                </h5>
                <div className="row"  id="row">
                    <div className="col-6">
                        <div className="float-right"
                            style={{display:this.state.loading ? 'none' : 'block'}}>
                            <canvas ref="output" width={500} height={500} style={{ display: this.state.webcam ? 'block' : 'none' }}/>
                            {!this.state.webcam && <WeCamAccess askForAccess={() => this.askWebCam()}/>}
                        </div>
                        <div id="loader" style={{ display: !this.state.loading ? 'none' : 'block' }}>
                            <h3 id="loadTitle">Cargando modelo de Tensorflow ...</h3>
                            <ReactLoading type="cylon" color="grey" height={'20%'} width={'20%'} id="reactLoader"/>
                        </div>

                    </div>
                    <div className="col-6">
                        <canvas ref="babylon" width={500} height={500} />
                    </div>
                </div>
                <div className="float-right">
                    <canvas id="yellow" width="250" height="400"></canvas>
                </div>
                <div ref="description" id="description"/>
            </div>
        );
    }
}

function save() {
  data_head();
}


function view() {
  var head_points = JSON.parse(descContent2);
  var c=document.getElementById("yellow");
  var ctx=c.getContext("2d");
  ctx.beginPath();
  for(var i in head_points){
    console.log(head_points[i][0]["position"].x);
    new Promise(resolve => setTimeout(resolve, 30000));
    ctx.arc(head_points[i][0]["position"].x,head_points[i][0]["position"].y, 5, 0, 2 * Math.PI);
    ctx.fillStyle = "green";
    ctx.fill();
  }
}







const WeCamAccess = ({askForAccess}) => (
    <div id="webcamaccess" className="d-flex justify-content-center">
        <h3>No hay acceso a la camara</h3>
        <button onClick={askForAccess}>
            Acceso a la camara
        </button>
    </div>);

ReactDOM.render(
    <App />,
    document.getElementById('react-container')
);
